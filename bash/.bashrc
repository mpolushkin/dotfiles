#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Source the system-wide bashrc
if [ -f /etc/bash.bashrc ]; then
    . /etc/bash.bashrc
fi

# Source local bashrc if there is one
[[ -f ~/.bashrc_local ]] && . ~/.bashrc_local

# Enable 256-color mode
if [ -n "$DISPLAY" -a "$TERM" == "xterm" ]; then
    export TERM=xterm-256color
fi

# If starship is installed, use it
if command -v starship &> /dev/null; then
    eval "$(starship init bash)"
else
    # Otherwise use generated promptline from promptline.vim extension
    source ~/.shell_prompt.sh
fi

# Use direnv if available
if command -v direnv &> /dev/null; then
    eval "$(direnv hook bash)"
fi

############################################################
# Aliases                                                  #
############################################################

alias vim=nvim
alias vimdiff='nvim -d'

alias ls='ls --color=auto'
alias ll='ls -lh'
alias la='ls -a'
alias lla='ls -lha'

alias g='git status'
alias gd='git diff'
alias ga='git add'
alias gc='git commit'
alias gca='git commit --amend'
alias gco='git checkout'
alias gr='git reset'
alias gl='git log'
alias gg='git graph'
alias gga='git graph --all'

alias exat='exa -T --git-ignore'
alias t='tmux new -s "$(basename "$PWD")"'

############################################################
# Custom Commands                                          #
############################################################

# cd also lists directory contents
cd () {
    builtin cd "$@"
    if [ $? -eq 0 ]; then
        ls
    fi
}

# cd to git root
cdr () {
    local git_root=$(git rev-parse --show-toplevel) && cd "$git_root"
}

# Create directory and cd into it
mkd () {
    mkdir -p "$@"
    if [ "$#" -eq 1 ]; then
        cd "$1"
    fi
}

# Open file using default program
open () {
    (xdg-open "$@" &> /dev/null)
}

# Run command quietly in the background
run () {
    ("$@" &> /dev/null &)
}

# View document in read-only mode
view () {
    libreoffice --view "$@"
}

############################################################
# Program Settings
############################################################

# General
export EDITOR=nvim
export MANPAGER='nvim +Man!'
eval $(dircolors)

# fzf settings
export FZF_DEFAULT_COMMAND='fd --type f --hidden --no-ignore-vcs --exclude ".git" --color always .'
export FZF_DEFAULT_OPTS='--layout=reverse-list --preview='"'"'bat -n --theme=OneHalfDark --line-range=:$LINES --color=always {-1}'"'"' --ansi'

# cmake settings
export CMAKE_GENERATOR='Ninja Multi-Config'
export CMAKE_CONFIG_TYPE=Debug
export CMAKE_EXPORT_COMPILE_COMMANDS=yes

############################################################
# Random Fixes                                             #
############################################################

# Get rid of the 'accessibility bus' warning
export NO_AT_BRIDGE=1

# flashplugin fix for midori
export MOZ_PLUGIN_PATH="/usr/lib/mozilla/plugins"
