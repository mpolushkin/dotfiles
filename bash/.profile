#
# ~/.bash_profile
#

# Add user bin directories to PATH
if [ -d "$HOME/.local/bin" ]; then
    export PATH="$HOME/.local/bin:$PATH"
fi

if [ -d "$HOME/bin" ]; then
    export PATH="$HOME/bin:$PATH"
fi

# Configure rust/cargo
export CARGO_HOME="$HOME/.local/cargo"
if [ -d "$CARGO_HOME/bin" ]; then
    export PATH="$CARGO_HOME/bin:$PATH"
fi

# Source local profile if there is one
[[ -f ~/.profile_local ]] && . ~/.profile_local

# Configure pyenv
export PYENV_ROOT="$HOME/.local/pyenv"
if [ -d "$PYENV_ROOT/bin" ]; then
    export PATH="$HOME/.local/pyenv/bin:$PATH"
fi

if command -v pyenv &> /dev/null; then
    eval "$(pyenv init --path)"
    eval "$(pyenv init -)"
    # necessary to propagate pyenv shell function to child shell processes
    export -f pyenv
fi

# Configure poetry - it should be installed using the installer to work across
# multiple python versions. After installation, be sure to generate the
# completion files:
#   poetry completions bash > ~/.local/share/bash-completion/completions/poetry
export POETRY_HOME="$HOME/.local/poetry"
if [ -d "$POETRY_HOME/bin" ]; then
# Spelling out the full path like this and not indenting should prevent the
# poetry installer from adding it in again.
export PATH="$HOME/.local/poetry/bin:$PATH"
fi

# Source bashrc
[[ -f ~/.bashrc ]] && . ~/.bashrc
