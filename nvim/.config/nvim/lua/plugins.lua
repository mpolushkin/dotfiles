return {
  -- Useful plugin to show you pending keybinds.
  { "folke/which-key.nvim", opts = {} },

  {
    -- Adds git releated signs to the gutter, as well as utilities for managing changes
    "lewis6991/gitsigns.nvim",
    opts = {
      on_attach = function(bufnr)
        vim.keymap.set(
          "n",
          "<leader>gp",
          require("gitsigns").prev_hunk,
          { buffer = bufnr, desc = "[G]o to [P]revious Hunk" }
        )
        vim.keymap.set(
          "n",
          "<leader>gn",
          require("gitsigns").next_hunk,
          { buffer = bufnr, desc = "[G]o to [N]ext Hunk" }
        )
        vim.keymap.set(
          "n",
          "<leader>ph",
          require("gitsigns").preview_hunk,
          { buffer = bufnr, desc = "[P]review [H]unk" }
        )
      end,
    },
  },

  -- Seamlessly navigate vim and tmux splits
  { "sunaku/tmux-navigate" },

  -- Make plugins work with . command
  { "tpope/vim-repeat" },

  -- Read/write restricted files with suda://
  {
    "lambdalisue/suda.vim",
    init = function()
      vim.g.suda_smart_edit = 1
    end,
  },

  -- More flexible abbreviations, substitutions and search
  { "tpope/vim-abolish" },

  -- Unix file utilities as ex commands
  { "tpope/vim-eunuch" },

  -- Surroundings
  {
    "kylechui/nvim-surround",
    opts = {
      keymaps = {
        insert = false,
        insert_line = false,
        normal = "gs",
        normal_cur = "gss",
        normal_line = "gS",
        normal_cur_line = "gSS",
        visual = "gs",
        visual_line = "gS",
        delete = "ds",
        change = "cs",
        change_line = "cS",
      },
    },
  },

  -- Automatically insert closing pairs
  { "windwp/nvim-autopairs", opts = { map_c_w = true } },

  -- A variety of handy mappings
  { "tpope/vim-unimpaired" },

  -- Nicer search experience
  { "junegunn/vim-slash" },

  -- Jump to last edit location when opening buffer
  { "farmergreg/vim-lastplace" },

  -- move anywhere!
  { "ggandor/leap.nvim", config = function ()
    require('leap').add_default_mappings()
  end},

  { "NoahTheDuke/vim-just" },
}
