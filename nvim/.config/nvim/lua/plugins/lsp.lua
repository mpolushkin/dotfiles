return {
  {
    -- LSP Configuration & Plugins
    "neovim/nvim-lspconfig",
    dependencies = {
      -- Automatically install LSPs to stdpath for neovim
      { "williamboman/mason.nvim", opts = { PATH = "append" } },
      "williamboman/mason-lspconfig.nvim",

      -- For linters and formatters
      "jose-elias-alvarez/null-ls.nvim",
      "jay-babu/mason-null-ls.nvim",

      "folke/neodev.nvim", -- additional neovim lua configuration
      "mrcjkb/rustaceanvim",
    },

    config = function()
      --  This function gets run when an LSP connects to a particular buffer.
      local on_attach = function(client, bufnr)
        local map = function(keys, func, desc, modes)
          if desc then
            desc = "LSP: " .. desc
          end
          if not modes then
            modes = "n"
          end

          vim.keymap.set(modes, keys, func, { buffer = bufnr, desc = desc })
        end

        map("<leader>rn", vim.lsp.buf.rename, "[R]e[n]ame")
        map("<leader>ca", vim.lsp.buf.code_action, "[C]ode [A]ction")

        map("gd", vim.lsp.buf.definition, "[G]oto [D]efinition")
        map("gr", require("telescope.builtin").lsp_references, "[G]oto [R]eferences")
        map("gI", vim.lsp.buf.implementation, "[G]oto [I]mplementation")
        map("<leader>D", vim.lsp.buf.type_definition, "Type [D]efinition")
        map("<leader>ds", require("telescope.builtin").lsp_document_symbols, "[D]ocument [S]ymbols")
        map("<leader>ws", require("telescope.builtin").lsp_dynamic_workspace_symbols, "[W]orkspace [S]ymbols")
        map("<leader>f", vim.lsp.buf.format, "[F]ormat buffer", { "n", "x" })

        -- See `:help K` for why this keymap
        map("K", vim.lsp.buf.hover, "Hover Documentation")
        map("<C-k>", vim.lsp.buf.signature_help, "Signature Documentation")

        -- Lesser used LSP functionality
        map("gD", vim.lsp.buf.declaration, "[G]oto [D]eclaration")
        map("<leader>wa", vim.lsp.buf.add_workspace_folder, "[W]orkspace [A]dd Folder")
        map("<leader>wr", vim.lsp.buf.remove_workspace_folder, "[W]orkspace [R]emove Folder")
        map("<leader>wl", function()
          print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
        end, "[W]orkspace [L]ist Folders")

        vim.lsp.inlay_hint.enable(true, { bufnr = 0 })

        if client.name == "clangd" then
          map("<localleader>]", "<Cmd>ClangdSwitchSourceHeader<CR>", "Switch between source and header")
        end
      end

      -- Custom settings for language servers
      local servers = {
        lua_ls = {
          Lua = {
            workspace = { checkThirdParty = false },
            telemetry = { enable = false },
          },
        },

        pylsp = {
          pylsp = {
            configurationSources = { "flake8" },
            plugins = {
              flake8 = {
                enabled = true,
                maxLineLength = 88,
                extendIgnore = { "E203", "E701" },
              },
              pycodestyle = { enabled = false },
            },
          },
        },
      }

      -- nvim-cmp supports additional completion capabilities, so broadcast that to servers
      local capabilities = vim.lsp.protocol.make_client_capabilities()
      capabilities = require("cmp_nvim_lsp").default_capabilities(capabilities)

      -- Setup neovim lua configuration (must be run before configuring lsp servers
      require("neodev").setup()

      -- Ensure the servers above are installed
      local mason_lspconfig = require("mason-lspconfig")

      mason_lspconfig.setup({
        handlers = {
          function(server_name)
            require("lspconfig")[server_name].setup({
              capabilities = capabilities,
              on_attach = on_attach,
              settings = servers[server_name],
            })
          end,

          ["rust_analyzer"] = function()
            -- This is handled by rustaceanvim instead.
          end,
        },
      })

      vim.g.rustaceanvim = {
        server = {
          on_attach = function(client, bufnr)
            on_attach(client, bufnr)
            vim.api.nvim_create_autocmd("BufWritePre", {
              group = vim.api.nvim_create_augroup("rust_analyzer", { clear = true }),
              callback = function()
                vim.lsp.buf.format()
              end,
            })
          end,
        },
      }

      local null_ls = require("null-ls")
      null_ls.setup({
        -- don't use "Makefile" as a root marker
        root_dir = require("null-ls.utils").root_pattern(".null-ls-root", ".git"),
      })

      require("mason-null-ls").setup({
        ensure_installed = {},
        automatic_installation = false,
        handlers = {
          isort = function()
            null_ls.register(null_ls.builtins.formatting.isort.with({
              extra_args = { "--profile=black" },
            }))
          end,
          black = function()
            null_ls.register(null_ls.builtins.formatting.black.with({
              extra_args = {
                "--preview",
                "--enable-unstable-feature=string_processing",
                "--enable-unstable-feature=multiline_string_handling",
              },
            }))
          end,
          mypy = function()
            null_ls.register(null_ls.builtins.diagnostics.mypy.with({
              extra_args = { "--python-executable", "python" },
            }))
          end,
        },
      })
    end,
  },
}
