return {
  -- Fuzzy Finder (files, lsp, etc)
  {
    "nvim-telescope/telescope.nvim",
    branch = "0.1.x",
    dependencies = { "nvim-lua/plenary.nvim" },
    config = function()
      require("telescope").setup({
        defaults = {
          sorting_strategy = "ascending",
          layout_strategy = "flex",
          layout_config = { prompt_position = "top" },
          mappings = {
            i = {
              ["<C-j>"] = "move_selection_worse",
              ["<C-k>"] = "move_selection_better",
            },
          },
        },
        pickers = {
          find_files = {
            hidden = true,
          },
        },
      })

      -- My custom file pickers
      local function my_find_files(opts)
        opts = opts or {}
        opts["hidden"] = true
        if vim.fn.executable("rg") then
          local hard_ignore = { ".git/", "*.pyc" }
          local command = { "rg", "--files" }
          for _, glob in ipairs(hard_ignore) do
            table.insert(command, "-g")
            table.insert(command, "!" .. glob)
          end
          opts["find_command"] = command
        end
        require("telescope.builtin").find_files(opts)
      end
      vim.keymap.set("n", "<leader>o", my_find_files, { desc = "[O]pen file" })
      vim.keymap.set("n", "<leader>O", function()
        my_find_files({ no_ignore = true })
      end, { desc = "[O]pen file (no ignore)" })

      -- Quick access search mappings
      vim.keymap.set(
        "n",
        "<leader>?",
        require("telescope.builtin").oldfiles,
        { desc = "[?] Find recently opened files" }
      )
      vim.keymap.set(
        "n",
        "<leader><space>",
        require("telescope.builtin").buffers,
        { desc = "[ ] Find existing buffers" }
      )
      vim.keymap.set("n", "<leader>/", function()
        -- You can pass additional configuration to telescope to change theme, layout, etc.
        require("telescope.builtin").current_buffer_fuzzy_find(require("telescope.themes").get_dropdown({
          winblend = 10,
          previewer = false,
        }))
      end, { desc = "[/] Fuzzily search in current buffer" })

      -- General search mappings
      vim.keymap.set("n", "<leader>gf", require("telescope.builtin").git_files, { desc = "Search [G]it [F]iles" })
      vim.keymap.set("n", "<leader>sf", require("telescope.builtin").find_files, { desc = "[S]earch [F]iles" })
      vim.keymap.set("n", "<leader>sh", require("telescope.builtin").help_tags, { desc = "[S]earch [H]elp" })
      vim.keymap.set("n", "<leader>sw", require("telescope.builtin").grep_string, { desc = "[S]earch current [W]ord" })
      vim.keymap.set("n", "<leader>sg", require("telescope.builtin").live_grep, { desc = "[S]earch by [G]rep" })
      vim.keymap.set("n", "<leader>sd", require("telescope.builtin").diagnostics, { desc = "[S]earch [D]iagnostics" })
      vim.keymap.set(
        "n",
        "<leader>st",
        require("telescope.builtin").treesitter,
        { desc = "[S]earch [T]reesitter symbols" }
      )
      vim.keymap.set(
        "n",
        "<leader>sb",
        require("telescope.builtin").builtin,
        { desc = "[S]earch telescope [B]uiltins" }
      )

      -- Additional mappings
      -- vim.keymap.set('n', '<Leader>eV', ':e <C-R>=resolve(expand("~/.config/nvim/"))<CR>', { desc = '[E]dit [N]eovim config ...' })
      vim.keymap.set("n", "<Leader>eV", function()
        my_find_files({ cwd = vim.fn.resolve(vim.fn.expand("~/.config/nvim")) })
      end, { desc = "[E]dit [N]eovim config ..." })
    end,
  },

  -- Fuzzy Finder Algorithm which requires local dependencies to be built.
  {
    "nvim-telescope/telescope-fzf-native.nvim",
    dependencies = { "nvim-telescope/telescope.nvim" },
    build = "make",
    cond = function()
      return vim.fn.executable("make") == 1
    end,
    config = function()
      require("telescope").load_extension("fzf")
    end,
  },
}
