return {
  -- Statusline
  {
    "nvim-lualine/lualine.nvim",
    dependencies = { "nvim-tree/nvim-web-devicons" },
    config = function()
      if vim.fn.TabooTabTitle then
        local Tab = require("lualine.components.tabs.tab")
        ---@diagnostic disable-next-line: duplicate-set-field
        Tab.label = function(self)
          return vim.fn.TabooTabTitle(self.tabnr)
        end
      end

      require("lualine").setup({
        options = {
          theme = "onedark",
          component_separators = "",
          section_separators = "",
        },
        sections = {
          lualine_b = { "diff", "diagnostics" },
          lualine_c = {
            { "filename", path = 1 },
          },
        },
        inactive_sections = {
          lualine_c = {
            { "filename", path = 1 },
          },
        },
        tabline = {
          lualine_a = {
            { "tabs", max_length = vim.o.columns, mode = 2 },
          },
        },
      })

      vim.opt.showtabline = 1
    end,
  },

  -- Rename tab pages
  {
    "gcmt/taboo.vim",
    config = function()
      vim.g.taboo_tabline = 0
      vim.g.taboo_tab_format = "%P [%S]%U"
      vim.g.taboo_renamed_tab_format = "%l [%S]%U"
    end,
  },

  -- Indentation guides
  {
    "lukas-reineke/indent-blankline.nvim",
    main = "ibl",
    config = true,
  },

  -- Default colorscheme
  {
    "navarasu/onedark.nvim",
    priority = 1000,
    config = function()
      vim.cmd.colorscheme("onedark")
    end,
  },

  -- Some additional color schemes
  { "shaunsingh/solarized.nvim" },
  { "Shatur/neovim-ayu" },
  { "sainnhe/everforest" },
  { "shaunsingh/nord.nvim" },
  { "ellisonleao/gruvbox.nvim" },
  { "catppuccin/nvim" },
}
