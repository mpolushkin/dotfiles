return {
  "L3MON4D3/LuaSnip",
  version = "1.*",
  build = "make install_jsregexp",

  config = function()
    local luasnip = require("luasnip")

    require("luasnip.loaders.from_vscode").lazy_load()
    require("luasnip.loaders.from_snipmate").lazy_load()
    luasnip.config.setup({})

    vim.keymap.set("i", "<C-l>", function()
      if luasnip.expand_or_locally_jumpable() then
        luasnip.expand_or_jump()
      else
        local keys = vim.api.nvim_replace_termcodes("<Esc>A", true, false, true)
        vim.api.nvim_feedkeys(keys, "m", false)
      end
    end)
    vim.keymap.set("i", "<C-h>", function()
      if luasnip.locally_jumpable(-1) then
        luasnip.jump(-1)
      end
    end)
  end,
}
