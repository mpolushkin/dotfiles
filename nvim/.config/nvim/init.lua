-- Configure global and local leader keys
--  NOTE: Must happen before plugins are required (otherwise wrong leader will be used)
vim.g.mapleader = " "
vim.g.maplocalleader = "\\"

-------------------------------------------------------------------------------
--  Options
-------------------------------------------------------------------------------
--------------------------- General -------------------------------------------
vim.opt.undofile = true -- Enable persistent undo
vim.opt.gdefault = true -- Substitute all matches on line instead of just 1

vim.o.timeout = true
vim.o.timeoutlen = 500

--------------------------- Neovim UI -----------------------------------------
vim.opt.termguicolors = true -- Enable truecolor support in terminal UI

vim.opt.mouse = "a" -- Enable mouse in all modes

vim.opt.number = true -- Show line numbers
vim.opt.relativenumber = true -- Make line numbers relative to current position

vim.opt.showmatch = true -- Show matching brackets

vim.opt.ignorecase = true -- Make search case-insensitive
vim.opt.smartcase = true -- unless pattern contains upper-case letters

vim.opt.scrolloff = 3 -- Show additional lines when scrolling off-screen

vim.opt.textwidth = 80
vim.opt.cursorline = true -- Highlight the line the cursor is currently on
vim.opt.colorcolumn = "+1" -- Color the column after 'textwidth'

vim.opt.splitbelow = true -- New horizontal splits appear below current window
vim.opt.splitright = true -- New vertical splits appear right of current window

vim.o.breakindent = true -- Match indentation of wrapped lines

vim.opt.listchars = { -- Pretty markings when 'list' is on
  tab = "→ ",
  eol = "↲",
  nbsp = "␣",
  space = "·",
  trail = "•",
  extends = "»",
  precedes = "«",
}

--------------------------- Editing -------------------------------------------
vim.opt.tabstop = 4 -- Representation of a tab character
vim.opt.shiftwidth = 4 -- Tabwidth when using < and >
vim.opt.expandtab = true -- Use spaces instead of tabs

vim.opt.inccommand = "nosplit" -- Live substitution

-- Set completeopt to have a better completion experience
vim.o.completeopt = "menuone,noselect"

-- Don't auto-wrap at 'textwidth'
vim.opt.formatoptions:remove { "t" }

-------------------------------------------------------------------------------
--  Mappings
-------------------------------------------------------------------------------
-- We use space as the mapleader, so disable its default action
vim.keymap.set({ "n", "x" }, "<Space>", "<Nop>", { silent = true })

-- Remap for dealing with word wrap
vim.keymap.set("n", "k", "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
vim.keymap.set("n", "j", "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })

-- Navigate between splits
vim.keymap.set("n", "<A-h>", "<C-W>h", { silent = true })
vim.keymap.set("n", "<A-j>", "<C-W>j", { silent = true })
vim.keymap.set("n", "<A-k>", "<C-W>k", { silent = true })
vim.keymap.set("n", "<A-l>", "<C-W>l", { silent = true })

-- Clear search highlight
vim.keymap.set("n", "<Leader>n", "<Cmd>nohl<CR>", { silent = true, desc = "Clear search highlight" })

-- More convenient access to clipboard
vim.keymap.set({ "n", "x" }, "<Leader>y", '"+y')
vim.keymap.set({ "n", "x" }, "<Leader>p", '"+p')
vim.keymap.set({ "n", "x" }, "<Leader>P", '"+P')

-- Yank up to end of line
vim.keymap.set("n", "Y", "yg_")
vim.keymap.set("n", "<Leader>Y", '"+yg_')

-- Quickly edit various config files
vim.keymap.set(
  "n",
  "<Leader>ev",
  ':e <C-R>=resolve(expand("~/.config/nvim/init.lua"))<CR><CR>',
  { desc = "[E]dit [N]eovim init.lua" }
)
vim.keymap.set(
  "n",
  "<Leader>eV",
  ':e <C-R>=resolve(expand("~/.config/nvim/"))<CR>',
  { desc = "[E]dit [N]eovim config ..." }
)
vim.keymap.set("n", "<Leader>ep", ':e <C-R>=resolve(expand("~/.profile"))<CR><CR>', { desc = "[E]dit ~/.[P]rofile" })
vim.keymap.set(
  "n",
  "<Leader>eP",
  ':e <C-R>=resolve(expand("~/.profile_local"))<CR><CR>',
  { desc = "[E]dit ~/.[P]rofile_local" }
)
vim.keymap.set("n", "<Leader>eb", ':e <C-R>=resolve(expand("~/.bashrc"))<CR><CR>', { desc = "[E]dit ~/.[B]ashrc" })
vim.keymap.set(
  "n",
  "<Leader>eB",
  ':e <C-R>=resolve(expand("~/.bashrc_local"))<CR><CR>',
  { desc = "[E]dit ~/.[B]ashrc_local" }
)
vim.keymap.set(
  "n",
  "<Leader>et",
  ':e <C-R>=resolve(expand("~/.tmux.conf"))<CR><CR>',
  { desc = "[E]dit ~/.[T]mux.conf" }
)
vim.keymap.set(
  "n",
  "<Leader>eg",
  ':e <C-R>=resolve(expand("~/.config/git/config"))<CR><CR>',
  { desc = "[E]dit global [G]itconfig" }
)
vim.keymap.set(
  "n",
  "<Leader>eG",
  ':e <C-R>=resolve(expand("~/.gitconfig"))<CR><CR>',
  { desc = "[E]dit global [G]itconfig (local machine)" }
)
vim.keymap.set(
  "n",
  "<Leader>ei",
  ':e <C-R>=resolve(expand("~/.config/git/ignore"))<CR><CR>',
  { desc = "[E]dit global git[I]gnore" }
)

-- Quickly create new files in the same directory as current file
vim.keymap.set("n", "<Leader>en", ':e <C-R>=expand("%:h")<CR>/', { desc = "[E]dit [N]ew sibling file" })

-- Navigate through diagnostic messages
vim.keymap.set("n", "[d", vim.diagnostic.goto_prev, { desc = "Go to previous diagnostic message" })
vim.keymap.set("n", "]d", vim.diagnostic.goto_next, { desc = "Go to next diagnostic message" })
vim.keymap.set("n", "<leader>i", vim.diagnostic.open_float, { desc = "Show diagnostic [I]nfo" })
vim.keymap.set("n", "<leader>q", vim.diagnostic.setloclist, { desc = "Open diagnostics list" })

-------------------------------------------------------------------------------
--  Autocmds
-------------------------------------------------------------------------------
-- Highlight on yank
local init_group = vim.api.nvim_create_augroup("init", { clear = true })

vim.api.nvim_create_autocmd("TextYankPost", {
  callback = function()
    vim.highlight.on_yank({ timeout = 500 })
  end,
  group = init_group,
  pattern = "*",
})

vim.cmd([[
augroup init
     " Create parent directories when saving a file
     function! MkNonExDir(file, buf)
         if empty(getbufvar(a:buf, '&buftype')) && a:file!~#'\v^\w+\:\/'
             let dir=fnamemodify(a:file, ':h')
             if !isdirectory(dir)
                 call mkdir(dir, 'p')
             endif
         endif
     endfunction
     autocmd BufWritePre * :call MkNonExDir(expand('<afile>'), +expand('<abuf>'))
augroup END
]])

-------------------------------------------------------------------------------
--  Plugins
-------------------------------------------------------------------------------
-- Install package manager
--    https://github.com/folke/lazy.nvim
--    `:help lazy.nvim.txt` for more info
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup("plugins", {
  install = {
    colorscheme = { "onedark" },
  },
})
